#include <iostream>
#include "BinaryTree.h"

using namespace std;


int main()
{
    cout << "tree 1--------------------------------------------------" << endl;
    BinaryTree t1 = BinaryTree();
    BinaryTree::input(&t1);

    cout << "tree 2--------------------------------------------------" << endl;

    BinaryTree t2 = BinaryTree();
    BinaryTree::input(&t2);

    cout << "result--------------------------------------------------" << endl;

    bool result = true;
    BinaryTree::compare(&t1, &t2, &result);
    if (result) cout << "two trees are the same shape" << endl;
    else cout << "two trees are not the same shape" << endl;
    return 0;
}