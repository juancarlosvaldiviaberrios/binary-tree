#include <iostream>


class BinaryTree
{
    private:
        float value;
        BinaryTree *left;
        BinaryTree *right;

    public:
        BinaryTree(float = 0.0, BinaryTree* = nullptr, BinaryTree* = nullptr);
        static void input(BinaryTree* = nullptr);
        float getValue();
        BinaryTree* getLeft();
        BinaryTree* getRight();
        void setValue(float = 0.0);
        void setLeft(BinaryTree* = nullptr);
        void setRight(BinaryTree* = nullptr);
        static void compare(BinaryTree*, BinaryTree*, bool *);
};

BinaryTree::BinaryTree(float value, BinaryTree *left, BinaryTree *right)
{
    this->value = value;
    this->left = left;
    this->right = right;
}

void BinaryTree::input(BinaryTree* root)
{
    float value;
    std::cout << "value: ";
    std::cin >> value;
    (*root).setValue(value);

    int is_node = 0;

    std::cout << "node " << (*root).getValue() << " has left? (1/0): ";
    std::cin >> is_node;
    if(is_node==1)
    {
        BinaryTree *left = new BinaryTree;
        (*root).setLeft(left);
        BinaryTree::input((*root).getLeft());
    }

    std::cout << "node " << (*root).getValue() << " has right? (1/0): ";
    std::cin >> is_node;
    if(is_node==1)
    {
        BinaryTree *right = new BinaryTree;
        (*root).setRight(right);
        BinaryTree::input((*root).getRight());
    }
    
}

float BinaryTree::getValue()
{
    return this->value;
}

BinaryTree* BinaryTree::getLeft()
{
    return this->left;
}

BinaryTree* BinaryTree::getRight()
{
    return this->right;
}

void BinaryTree::setValue(float value)
{
    this->value = value;
}

void BinaryTree::setLeft(BinaryTree* left)
{
    this->left = left;
}

void BinaryTree::setRight(BinaryTree* right)
{
    this->right = right;
}

void BinaryTree::compare(BinaryTree* t1, BinaryTree* t2, bool *result)
{
    if (!(*result))
    {
        return;
    }
    if (!(t1 || t2))
    {
        return;
    }
    if (!(t1 && t2))
    {
        *result = false;
        return;
    }
    BinaryTree::compare((*t1).getLeft(), (*t2).getLeft(), result);
    BinaryTree::compare((*t1).getRight(), (*t2).getRight(), result);
}
